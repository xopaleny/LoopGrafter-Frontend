VERSION=1.0.0.0
PKGNAME=loopgrafter-frontend-$(VERSION)

all:

dist:
	mkdir -p dist/$(PKGNAME)
	cp -r babel.config.js package.json package-lock.json README.md vue.config.js public/ src/ dist/$(PKGNAME)
	tar czf dist/$(PKGNAME).tar.gz -C dist $(PKGNAME)
	rm -fr dist/$(PKGNAME)

clean:
	rm -rf dist/
