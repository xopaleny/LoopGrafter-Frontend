## Quick start

### requriments
 - use Node v16 LTS

### install dependencies
```
npm ci
```
### set up .env file in the root directory

```
VUE_APP_LOOPGRAFTER_BACKEND_URL=<url>
VUE_APP_LOOPGRAFTER_DIRECT_API_MAPPING=ano_prosim

VUE_APP_INFO_BANNER_URL_1="https://example.com/image1.png"
VUE_APP_INFO_BANNER_LINK_1="https://example.com/"
VUE_APP_INFO_BANNER_URL_2="https://example.com/image2.png"
VUE_APP_INFO_BANNER_LINK_2="https://example2.com/"
VUE_APP_INFO_BANNER_URL_3="https://example.com/image3.png"
VUE_APP_INFO_BANNER_LINK_3="https://example3.com/"
VUE_APP_INFO_BANNER_URL_4="https://example.com/image4.png"
VUE_APP_INFO_BANNER_LINK_4="https://example4.com/"
```

### run
```
npm run serve
```

Refer to `https://cli.vuejs.org/` for additional support 
