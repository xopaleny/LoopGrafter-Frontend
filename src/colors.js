import { color,rgb } from 'd3-color';

export default {
  selected: function(c){

    let colorObject = color(c);
    if(Array.isArray(c)){

      colorObject = rgb(c[0] *255, c[1] *255, c[2]*255);
    }
    
    return color(colorObject.brighter(1.2)).formatHex();
  }
}