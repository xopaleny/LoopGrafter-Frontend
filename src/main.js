import Vue from "vue";
import store from "./store";
import router from "./router"
import App from "./App.vue";
import VueLog from '@dreipol/vue-log';
import VueTour from 'vue-tour'
require('vue-tour/dist/vue-tour.css')

Vue.use(VueTour)

Vue.use(VueLog);
Vue.config.productionTip = false;


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");


