import { color } from 'd3';

export default {
  methods: {
    indexPerProtein(index) {
      let protein = this.details[index].protein;
      let indexPP = 0;
      for(let i = 0; i < index; i++){
        if(this.details[i].protein.name == protein.name){
          indexPP++;
        }
      }
      return indexPP;
    },

    nextColor(proteinColor, index) {
      let ipp = this.indexPerProtein(index);
      return color(proteinColor)
        .darker(-1)
        .darker( ipp * (Math.log(Math.log(ipp + 1)+1)/Math.log(1.5)))
        .formatHex();
    },

  },
}