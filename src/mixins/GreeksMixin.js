import * as d3 from "d3";



export default {

  methods: {
    calculateAngleColor(angle) {
      return { background: this.angleGrey(angle) };
    },
    calculateDistanceColor(distance) {
      return { background: this.distanceGrey(distance) };
    },
    calculateRhoAngleColor(rho) {
      return { background: this.angleFullGrey(rho) };
    },

  },

  computed: {
    colorRange: function() {
      return [d3.rgb("#222222"), d3.rgb("#D0D0D0")]
    },
    distanceGrey: function () {
      return d3
        .scaleLinear()
        .domain([this.distances.min, this.distances.max])
        .interpolate(d3.interpolateHcl)
        .range(this.colorRange)
    },
    distances: function () {
      let distances = this.$store.getters.getAllLoops.filter(l => l.hasGeometry).map(l => Number(l.distance));
      return { min: Math.min(...distances), max: Math.max(...distances) };
    },
    angleGrey: function () {
      return d3
        .scaleLinear()
        .domain([0, 180])
        .interpolate(d3.interpolateHcl)
        .range(this.colorRange)
    },
    angleFullGrey: function () {
      return  d3.scaleLinear()
      .interpolate(d3.interpolateRgb.gamma(1.5))
      .domain([0, 90, 270, 360])
      .range([d3.rgb("#99221f"), d3.rgb("#333333"), d3.rgb("#CCCCCC"), d3.rgb("#99221f")]);
      // return d3
      //   .scaleLinear()
      //   .domain([0, 360])
      //   .interpolate(d3.interpolateHcl)
      //   .range([ d3.rgb("#333333"), d3.rgb("#CCCCCC"), d3.rgb(this.protein.color), d3.rgb("#CCCCCC"), d3.rgb("#333333")])
    }
  }
}