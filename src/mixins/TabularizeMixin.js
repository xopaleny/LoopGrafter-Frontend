import { differenceBy } from "lodash"
export default {

    methods: {
        tabularizeSegments(segments, beginSelector, endSelector, touch) {
            function near(loop1, loop2) {
                return Math.abs(beginSelector(loop1) - endSelector(loop2)) < (touch ? 1 : 2) || Math.abs(beginSelector(loop2) - endSelector(loop1)) < (touch ? 1 : 2);
            }
            
            let sortedSegments = [...segments].sort((l1, l2) => beginSelector(l1) - beginSelector(l2));
            let segmentSets = [];
            while (sortedSegments.length != 0) {
                let currentSet = [];
                let lastSegment = { end: Number.NEGATIVE_INFINITY, to: Number.NEGATIVE_INFINITY }
                for (let segment of sortedSegments) {
                    if (!near(segment, lastSegment) && endSelector(lastSegment) < beginSelector(segment)) {
                        currentSet.push(segment);
                        lastSegment = segment;
                    }
                }
                
                if(currentSet.length == 0){
                    debugger;
                    throw "2D loop vizualizaiton failed to compute";
                }
                sortedSegments = differenceBy(sortedSegments, currentSet);
                segmentSets.push(currentSet);
            }

            return segmentSets;
        }
    }
}

