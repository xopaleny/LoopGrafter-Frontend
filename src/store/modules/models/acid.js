export default class Acid {
  constructor(index, chain, pdb_number, secondary_structure, acid_name) {
    this.index = index;
    this.chain = chain;
    this.pdb_number = pdb_number;
    this.acid_name = acid_name;
    this.secondary_structure = secondary_structure;
    this.bfactors = {
      residue: {},
      secondary_structure: {}
    };
  }

  get simple_secondary_structure() {
    return this.simplify(this.secondary_structure);
  }

  get simple_coils_secondary_structure() {
    return this.simplifyCoils(this.secondary_structure);
  }

  simplify(secondary_structure) {
    if (['G', 'H', 'I'].includes(secondary_structure)) { // helix
      return 'H';
    } else if (['E'].includes(secondary_structure)) { // list
      return 'E';
    } else if (['B', 'T', 'S', 'C', '-', ' '].includes(secondary_structure)) { // other, coil
      return 'C';
    }
  }

  simplifyCoils(secondary_structure) {
    if (['B', 'T', 'S', 'C', '-', ' '].includes(secondary_structure)) { // other, coil
      return 'C';
    }
    return secondary_structure;
  }

  nukeFlexibility() {
    this.bfactors = {
      residue: {},
      secondary_structure: {}
    };
  }
}