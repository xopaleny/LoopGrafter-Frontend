export default class Step {
    constructor(id, label){
        this.id = id;
        this.label = label
    }
    get description(){
        // return file;
        let des = require(`../../../assets/resources/workflow-description/${this.id}.res.htm`);
        return des.default;
    } 
}