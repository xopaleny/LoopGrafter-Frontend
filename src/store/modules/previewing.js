export default {
  state: {
    preview: null,
    loop: null
  },
  getters: {
    getPreview(state) {
      return state.preview;
    },
    getSelectedLoop(state) {
      return state.loop;
    }
  },
  mutations: {
    setPreview(state, preview) {
      state.preview = preview;
      if (preview != null) {
        let { description } = preview;
        state.preview.description = description;
      }
    },
    setLoopPreview(state, loop) {
      state.loop = loop
    }
  },
  actions: {

  }
}