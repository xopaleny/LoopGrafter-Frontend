import _ from "lodash";

export default {
  state: {
    awaitables: [],

  },
  getters: {
    getCurrentAwaitables(state) {
      return state.awaitables;
    },

    isAwaiting(state) {
      return state.awaitables.length > 0;
    },
  },
  mutations: {
    addAwaitable(state, awaitable) {
      state.awaitables.push(awaitable);
    },
    removeAwaitable(state, awaitable) {
      state.awaitables = state.awaitables.filter((a) => a.id != awaitable.id)
    },



  },
  actions: {
    addAwaitable(context, awaitable) {
      context.commit('addAwaitable', awaitable);
      awaitable.promise.finally(function () {
        context.commit('removeAwaitable', awaitable);
      })
    },

  }
}