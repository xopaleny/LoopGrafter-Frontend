import _ from "lodash";
import Step from "./models/step";
let steps = [
    new Step(
        "protein-selection",
        "Secondary Structures",
    ),
    new Step(
        "loop-exploration",
        "Loop Exploration"
    ),
    new Step(
        "flexibility-filter",
        "Flexibility Evaluation"
    ),
    new Step(
        "crosscorelation-filter",
        "Correlation Evaluation"
    ),
    new Step(
        "grafting",
        "Loop Grafting"
    ),
    new Step(
        "loop-pairing",
        "Loop Pairing"
    ),
];

export default {
    state: {
        currentStepIndex: 0,
        exampleMode: false
    },
    getters: {
        exampleMode(state, getters, rootState) {
            return state.exampleMode;
        },
        getCurrentWorkflowStep(state, getters, rootState) {
            return steps[state.currentStepIndex];
        },
        getWorkflowStep(state) {
            return (step_id) => steps.find((s) => s.id == step_id);
        },
        loopExplorationAvailable(state, getters) {
            return getters.getScaffold != null && getters.getInsert != null;
        },
        flexibilityFilterAvailable(state, getters) {
            return getters.getScaffold != null && getters.getScaffold.loops.length > 0 && !getters.getScaffold.containsIncorrectLoops();
        },
        crosscorelationFilterAvailable(state, getters) {
            return getters.getScaffold != null && getters.getScaffold.availableCcors.find((ctype) => ctype == 'anm') != null
        },
        pairingAvailable(state, getters) {
            return getters.getScaffold != null && getters.getInsert != null && getters.getWantedLoops.length > 0 && getters.getInsert.loops.length > 0;
        },
        graftingAvailable(state, getters) {
            return getters.getWantedLoops.length > 0 && _.every(getters.getWantedLoops, (l => l.replacement != null));
        },


    },
    mutations: {
        nextWorkflowStep(state) {
            state.currentStepIndex++;
        },
        goToWorkflowStep(state, step) {
            let newStep = steps.findIndex((s) => s.id == step);
            if (newStep == -1) {
                throw `Step ${step} does not exist`;
            }
            state.currentStepIndex = newStep;
        },
        setExampleMode(state, enabled) {
            state.exampleMode = enabled;
        },
        nukeLoopExploartionData(state, { protein }) {
            protein.nukeLoops()
        },
        nukeFlexibility(state, { protein }) { //and crosscorellation
            protein.nukeFlexibility()
            protein.nukeCorrelation()
        },
        nukeGrafting(state) {
            state.pregraftData = null;
        }

    },
    actions: {
        nukeLoopExploartionData(context, { role }) {
            let protein = context.getters.getRole(role);
            if (role == "scaffold") {
                for (let loop of protein.loops.concat(protein.superloops)) {
                    context.commit("deleteInterestingLoop", loop);
                }
                context.dispatch("nukeFlexibility");
            }

            context.commit("nukeLoopExploartionData", { protein })
            context.dispatch("nukePairing");


        },
        nukeFlexibility(context) { //and crosscorellation
            let protein = context.getters.getScaffold;
            context.commit("nukeFlexibility", { protein })
        },
        nukePairing(context) {
            let scaffold = context.getters.getScaffold;

            for (let loop of scaffold.allLoops) {
                context.dispatch("removePairedInsert", loop)
            }
            context.dispatch("nukeGrafting");
        },
        nukeGrafting(context) {
            context.commit("nukeGrafting")
        }
    }
}